// написати функцію яка приймає в параметри дату та число яке повино представляти кількість днів
// реалізувати можливість передачі додатнього числа для додавання та відємного для віднімання

function days(callback, date, numDay, num) {

    date.setDate(callback(numDay, num));
    return date
}

let date = new Date();

console.log(days((numDay, num) => {
    if (Math.sign(num) !== 1)
        return numDay -= Math.abs(num);
    else return numDay += num;
}, date, 5, -8));
